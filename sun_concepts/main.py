import pandas as pd
import common
import json

tax = json.loads(common.get_taxonomy_concepts())
concepts = tax["data"]["concepts"]
tax_list = [x for x in concepts if x["type"] in ["keyword", "language", "skill", "occupation-name"]]
print("types to consider:", {'keyword', 'language', 'skill', 'occupation-name'})
print("labels to consider:", {'preferred_label'})

#tax_list = tax_list[0:50]
#print(tax_list)

sun_concepts_list = []
sun_concepts_file = pd.read_excel('../data/sun-possible-relations.xlsx', usecols=['preferred_label'])
sun_concepts = [item.lower() for sublist in sun_concepts_file.values for item in sublist]
sun_concepts_list.append(sun_concepts)

#sun_keyword_file = pd.read_excel('sun-code-keyword.xlsx', usecols=['Preferred-label'])
#sun_keyword = [item.lower() for sublist in sun_keyword_file.values for item in sublist]
#sun_concepts_list.append(sun_keyword)

sun_concepts_list = common.flatten_list(sun_concepts_list)
sun_concepts_list = common.remove_duplicates(sun_concepts_list)

common.get_connected_concepts_ods(tax_list, sun_concepts_list)



