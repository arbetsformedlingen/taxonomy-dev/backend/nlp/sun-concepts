sentence_transformers==2.2.2
scikit-learn==1.3.1
numpy~=1.26.0
Jinja2==3.1.4
pandas==2.2.2
pyexcel_ods==0.6.0
openpyxl==3.1.2

