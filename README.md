# Sun Concepts

This repository contains possible relations between sun concepts and other concepts in Taxonomy to help Redaction Team. We used Sentence-BERT model to find relations between them.

## Set up environment

- Make sure python 3.11 is installed.
- Make sure pip is installed.
- Set up a venv (IntelliJ or similar environment):
  - File -> Project Structure -> SDKs -> + -> Add Python SDK... -> Virtual ENV Environment
- Install dependencies:
  `pip install - r requirements.txt`

## Expected result:

`sun_connected_concepts.ods` is the expected result when you run `main.py`.
