import unittest
import os, sys
sys.path.insert(0, os.path.abspath("../sun_concepts"))
import common
import main

class MyTestCase(unittest.TestCase):
    def test_tax_list(self):
        preferred_label = [item['preferred_label'] for item in  main.tax_list]

        self.assertTrue('Grekiska' in preferred_label)
        self.assertTrue('Danska' in preferred_label)
        self.assertTrue('Azerbajdzjanska/Azeriska' in preferred_label)
        self.assertTrue('C++ Certified Associate Programmer Certification' in preferred_label)

        self.assertFalse('Sara' in preferred_label)
        self.assertFalse('mekatronik' in preferred_label)
        self.assertFalse('biomedicin' in preferred_label)
        self.assertFalse('juridik' in preferred_label)

    def test_sun_concepts_list(self):
        sun_concepts = main.sun_concepts_list

        self.assertTrue('mekatronik' in sun_concepts)
        self.assertTrue('biomedicin' in sun_concepts)
        self.assertTrue('juridik' in sun_concepts)
        self.assertTrue('spanska' in sun_concepts)

        self.assertFalse('Sara' in sun_concepts)
        self.assertFalse('Grekiska' in sun_concepts)
        self.assertFalse('Danska' in sun_concepts)
        self.assertFalse('C++ Certified Associate Programmer Certification' in sun_concepts)

if __name__ == '__main__':
    unittest.main()

